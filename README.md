# README #

### What is this repository for? ###

* This is an example of what can be created with the twitter api to measure keyword volumes
* It is part of a case study that explores the api
* the lecture notes can be found in the Goldsmith Lecture Notes directory in keynote format

### How do I get set up? ###

* clone the repository
* npm install
* node app.js
$(document).ready(initMap);

var map, heatmap;
var points = new google.maps.MVCArray();

var socket = io.connect();

socket.on('tweet' , function(tweet){

  var tweetLocation = new google.maps.LatLng(tweet.coordinates.coordinates[1], tweet.coordinates.coordinates[0]);

  points.push(tweetLocation);

  //Flash a dot onto the map quickly
  var marker = new google.maps.Marker({
    position: tweetLocation,
    map: map
  });
  setTimeout(function(){
    marker.setMap(null);
  },600);

});


function initMap() {
  map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 5,
    center: {lat: 37.090240, lng: -95.712891},
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: points,
    map: map
  });
}
